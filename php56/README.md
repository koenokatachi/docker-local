## php5.6 cli image

### Provides
- php5.6 from ppa:ondrej/php with some stock extensions
- pecl mongo extension
- cld extension (=Chrome Language Detector)
- composer at /usr/local/bin
