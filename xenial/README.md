## Base image

### Requirements

Host /etc/docker/daemon.json
```json
{
	"bip": "172.17.0.1/24"
}
```

### Provides
- apt is proxied to apt-cacher-ng on host
- apt sources are switched to DE mirror
- packages: locales, ca-certificates
- en_US.UTF-8 is generated and set as default
- 1000:1000 user created

